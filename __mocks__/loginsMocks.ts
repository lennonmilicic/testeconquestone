import { Observable, Observer } from 'rxjs';

export default {
    login: (username: string, password: string) => {
        return new Observable((observer: Observer<any>) => {
            if (username === 'usuarioa@email.com.br' && password === 'Usuario@1') {
                observer.next('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IlVzdWFyaW8gQSIsImlhdCI6MTUxNjIzOTAyMiwicHJvZmlsZSI6eyJQYWluZWxBIjp0cnVlLCJQYWluZWxCIjpmYWxzZX19.dsqIpW6duMIECRQ5SarBZtKTX4Mv0F7TzWcdjxnarNs');
                observer.complete();
            } else if (username === 'usuariob@email.com.br' && password === 'Usuario@1') {
                observer.next('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IlVzdWFyaW8gQiIsImlhdCI6MTUxNjIzOTAyMiwicHJvZmlsZSI6eyJQYWluZWxBIjpmYWxzZSwiUGFpbmVsQiI6dHJ1ZX19.wFg5fQaDQFoUCHFIMLomRun7Uat9fpVJmemDwE3iaOo');
                observer.complete();
            } else if (username === 'usuarioc@email.com.br' && password === 'Usuario@1') {
                observer.next('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IlVzdWFyaW8gQyIsImlhdCI6MTUxNjIzOTAyMiwicHJvZmlsZSI6eyJQYWluZWxBIjp0cnVlLCJQYWluZWxCIjp0cnVlfX0.L2K_966jt1Oz2xT4xCXGnWhh52OWEQ4_oSkvdFexaGc');
                observer.complete();
            } else if (username === 'usuariod@email.com.br' && password === 'Usuario@1') {
                observer.next('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IlVzdWFyaW8gRCIsImlhdCI6MTUxNjIzOTAyMiwicHJvZmlsZSI6eyJQYWluZWxBIjpmYWxzZSwiUGFpbmVsQiI6ZmFsc2V9fQ.uM4SiOnMvBDALSNDf6v7orZvvOLMjYNH6sWSNcDkdXQ');
                observer.complete();
            } else {
                observer.error({
                    message: 'Usuario não encontrado.'
                });
                observer.complete();
            }
        });
    },
};
