import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { GlobalService } from 'src/app/services/global.service';
import * as jwtDecode from 'jwt-decode';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

    /**
     * Regex para email
     */
    checkEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

    /**
     * Regex para senha
     */
    checkPassword = /^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;

    /**
     * Variavel do Modal de Login
     */
    modalShow = false;

    constructor(
        private readonly loginService: LoginService,
        private readonly globalService: GlobalService,
        private readonly router: Router,
    ) { }

    ngOnInit() { }

    /**
     * Função de login
     * @param NgForm do FormLogin
     */
    logar = (form: NgForm) => {
        if (form.invalid) { return; }
        const { username, password } = form.value;
        this.loginService.login(username, password).subscribe(
            (success: string) => {
                this.globalService.TokenJwt = success;
                this.globalService.UserData = jwtDecode(this.globalService.TokenJwt);
                this.router.navigateByUrl('/dashboard');
            },
            (fail) => alert(fail.message)
        );
    }

}
