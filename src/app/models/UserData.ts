class ProfileData {
    public PainelA: boolean;
    public PainelB: boolean;

    constructor(PainelA: boolean, PainelB: boolean) {
        this.PainelA = PainelA;
        this.PainelB = PainelB;
    }
}

export class UserData {
    public name: string;
    public profile: ProfileData;

    constructor(name: string, profile: ProfileData) {
        this.name = name;
        this.profile = profile;
    }
}
