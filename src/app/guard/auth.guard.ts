import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { UserData } from '../models/UserData';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(
        private readonly router: Router,
        private readonly globalService: GlobalService,
    ) { }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (this.globalService.TokenJwt) {
            return true;
        } else {
            this.globalService.UserData = {} as UserData;
            this.globalService.TokenJwt = '';
            this.router.navigateByUrl('');
            return false;
        }
    }
}
