import { Injectable } from '@angular/core';
import { UserData } from '../models/UserData';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class GlobalService {

    private static tokenJwt: string;
    get TokenJwt() { return GlobalService.tokenJwt; }
    set TokenJwt(value: string) { GlobalService.tokenJwt = value; }

    private static userData: UserData = {} as UserData;
    get UserData() { return GlobalService.userData; }
    set UserData(value: UserData) { GlobalService.userData = value; }

    constructor(
        private readonly router: Router,
    ) { }

    deslogar = () => {
        this.TokenJwt = '';
        this.UserData = {} as UserData;
        this.router.navigateByUrl('');
    }
}
