import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import loginsMocks from '__mocks__/loginsMocks';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    constructor() { }

    /**
     * Serviço para consumir API / MOCK no caso.
     */
    login = (username: string, password: string): Observable<string> => loginsMocks.login(username, password);
}
